# diana-core

## Requirements

-   Python 3 (tested on 3.6.4, 3.6.5, 3.6.6, 3.7.0) for development
-   [Flask](http://flask.pocoo.org/) as the web framework
-   [Gunicorn](https://gunicorn-docs.readthedocs.io) for WSGI HTTP server

[Pipenv](https://github.com/pypa/pipenv) is used in development to manage the environment and packages.

## Deploy

### Without Pipenv

    python setup.py install
    gunicorn service:app -b localhost:5000

### With Pipenv (recommanded)

    pipenv install -e .
    pipenv run gunicorn service:app -b localhost:5000

## API

Base URL: `http://localhost:5000`

### Message (`/logic/m`)

#### Request

-   Method: `GET`
-   Body:
    -   `msg`: the message text
    -   `uid`: the user's id given by LINE

#### Response

Check out **Request Body** of [LINE Messaging API Multicast Messages](https://developers.line.me/en/reference/messaging-api/#send-multicast-messages).
